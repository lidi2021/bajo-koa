import Router from "koa-router";

const userController = require("../controllers/users");
const homeController = require("../controllers/home");
const facilityController = require("../controllers/facility");

const {verifyToken} = require('../helpers/auth_helpers');

const router = new Router();

router.get(`/me`, verifyToken, userController.me);
router.get(`/home`, verifyToken, homeController.lists);
router.get(`/facilities`, verifyToken, facilityController.lists);

export default router;