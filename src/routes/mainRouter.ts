import Router from "koa-router";

const authController = require("../controllers/auth");

const router = new Router();

router.get(`/ping`, async (ctx) => {
    try {
        ctx.body = {
            status: "success",
            data: "pong"
        }
    } catch (e) {
        console.log(e);
    }
});



router.post(`/login`, authController.login);
router.post(`/register`, authController.register);


export default router;