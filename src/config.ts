module.exports = {
    tipe_fasilitas: ["Hotel","Food","Destination","Transport","Health","Store"],

    media_path_banner_original: (process.env.PATH_MEDIA || "") + "media/banner/original/",
    media_server_banner_original: (process.env.SERVERNAME_MEDIA || "")  + "media/banner/original/",
    media_path_banner_thumb: (process.env.PATH_MEDIA || "")  + "media/banner/original/",
    media_server_banner_thumb: (process.env.SERVERNAME_MEDIA || "")  + "media/banner/original/",

    media_path_information_original: (process.env.PATH_MEDIA || "") + "media/information/original/",
    media_server_information_original: (process.env.SERVERNAME_MEDIA || "")  + "media/information/original/",
    media_path_information_thumb: (process.env.PATH_MEDIA || "")  + "media/information/original/",
    media_server_information_thumb: (process.env.SERVERNAME_MEDIA || "")  + "media/information/original/",

    media_path_facility_original: (process.env.PATH_MEDIA || "") + "media/facility/original/",
    media_server_facility_original: (process.env.SERVERNAME_MEDIA || "")  + "media/facility/original/",
    media_path_facility_thumb: (process.env.PATH_MEDIA || "")  + "media/facility/original/",
    media_server_facility_thumb: (process.env.SERVERNAME_MEDIA || "")  + "media/facility/original/",

    back_images: (process.env.SERVERNAME_MEDIA || "")  + "template/backend_template/img/"
}