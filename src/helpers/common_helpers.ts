const { 
    media_path_banner_original,
    media_server_banner_original,
    media_path_banner_thumb,
    media_server_banner_thumb,
    media_path_information_original,
    media_server_information_original,
    media_path_information_thumb,
    media_server_information_thumb,
    media_path_facility_original,
    media_server_facility_original,
    media_path_facility_thumb,
    media_server_facility_thumb,
    back_images
 } = require("../config");

const pagination = (limit: number, page: number) => {
    if (!limit || limit < 1) {
        limit = 20;
    }

    if (!limit || limit > 200) {
        limit = 200;
    }

    if (!page || page < 1) {
        page = 1;
    }

    let offset = (page - 1) * limit;
    var result = {
        page: page,
        limit: limit,
        offset: offset,
    }
    return result;
}

const imageThumb = (path: string, dimension: string, type: string) => {
    let path_thumbs = "";
    let server_thumbs = "";
    let path_original = "";
    let server_original = "";
    switch (type) {
        case 'banner':
            path_thumbs     = media_path_banner_thumb;
            server_thumbs   = media_server_banner_thumb;
            path_original   = media_path_banner_original;
            server_original = media_server_banner_original;
            break;
        case 'information':
            path_thumbs     = media_path_information_thumb;
            server_thumbs   = media_server_information_thumb;
            path_original   = media_path_information_original;
            server_original = media_server_information_original;
            break;
        case 'facility':
            path_thumbs     = media_path_facility_thumb;
            server_thumbs   = media_server_facility_thumb;
            path_original   = media_path_facility_original;
            server_original = media_server_facility_original;
            break;
        default:
    }

    if(path != "") {
        let str_path = "";
        const exp_img:string[] = path.split('.');

        if(dimension != undefined && dimension != "") {
            str_path = server_thumbs + exp_img[0] + "_" + dimension + "_thumb." + exp_img[1];
        } else {
            str_path = server_original + path;
        }

        return str_path;

    } 

    return back_images + "no-image.jpg";
}

module.exports = {
    pagination,
    imageThumb
}