import { Context, Next } from "koa";
const jwt = require('jsonwebtoken');

async function verifyToken(ctx: Context, next: Next) {
    const authHeader = ctx.request.headers.authorization;
    console.log(authHeader);
    const token = authHeader && authHeader.split(' ')[1];
    if (token == null) {
        ctx.status = 401;
        ctx.body = { error: "Header authorization required" };
        return
    }

    try {
        const user = jwt.verify(token, process.env.JWT_SECRET_TOKEN);
        ctx.user = user;
    } catch (err) {
        ctx.status = 403;
        ctx.body = { error: "This data is protected, please login" };
        return
    }

    await next();
}

module.exports = {
    verifyToken
}