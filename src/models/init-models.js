var DataTypes = require("sequelize").DataTypes;
var _api_keys = require("./api_keys");
var _api_logs = require("./api_logs");
var _article = require("./article");
var _banner = require("./banner");
var _facilities = require("./facilities");
var _facilities_img = require("./facilities_img");
var _facilities_merchants = require("./facilities_merchants");
var _facilities_services = require("./facilities_services");
var _logs = require("./logs");
var _members = require("./members");
var _merchants = require("./merchants");
var _merchants_type = require("./merchants_type");
var _merchants_users = require("./merchants_users");
var _roles = require("./roles");
var _roles_modules = require("./roles_modules");
var _users = require("./users");

function initModels(sequelize) {
    var api_keys = _api_keys(sequelize, DataTypes);
    var api_logs = _api_logs(sequelize, DataTypes);
    var article = _article(sequelize, DataTypes);
    var banner = _banner(sequelize, DataTypes);
    var facilities = _facilities(sequelize, DataTypes);
    var facilities_img = _facilities_img(sequelize, DataTypes);
    var facilities_merchants = _facilities_merchants(sequelize, DataTypes);
    var facilities_services = _facilities_services(sequelize, DataTypes);
    var logs = _logs(sequelize, DataTypes);
    var members = _members(sequelize, DataTypes);
    var merchants = _merchants(sequelize, DataTypes);
    var merchants_type = _merchants_type(sequelize, DataTypes);
    var merchants_users = _merchants_users(sequelize, DataTypes);
    var roles = _roles(sequelize, DataTypes);
    var roles_modules = _roles_modules(sequelize, DataTypes);
    var users = _users(sequelize, DataTypes);

    facilities.belongsToMany(merchants, { through: facilities_merchants, foreignKey: 'id_facilities' });
    merchants.belongsToMany(facilities, { through: facilities_merchants, foreignKey: 'id_merchants' });

    return {
        api_keys,
        api_logs,
        article,
        banner,
        facilities,
        facilities_img,
        facilities_merchants,
        facilities_services,
        logs,
        members,
        merchants,
        merchants_type,
        merchants_users,
        roles,
        roles_modules,
        users,
    };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;