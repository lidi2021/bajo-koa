const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('roles_modules', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      primaryKey: true
    },
    id_roles: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    id_modules: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    modules_name: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    view: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    view_self: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    view_hub: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    add: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    modif: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    delete: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    },
    approval: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 0
    }
  }, {
    sequelize,
    tableName: 'roles_modules',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
    ]
  });
};
