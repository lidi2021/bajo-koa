import { Context } from "koa";

require('dotenv').config();

const jwt = require('jsonwebtoken');

async function me(ctx: Context) {
    try {
        ctx.body = {
            //token: ctx.token,
            data: ctx.user
        };
    } catch (e) {
        console.log(e);
    }
}

module.exports = {
    me
}