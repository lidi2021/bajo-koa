import { Context } from "koa";

require('dotenv').config();

const initModels = require("../models/init-models").initModels;
const connection = require("../storage/connection");

const models = initModels(connection);
const crypt = require('crypto');
const jwt = require('jsonwebtoken');

async function login(ctx: Context) {
    try {
        const params = ctx.request.body;
        const password_encrypted = crypt.createHash('md5').update(params.password).digest("hex");
        const member = await models.members.findAll({
            attributes: ['id','email','name'],
            where: {
                email: params.email.trim(),
                password: password_encrypted,
                status: 1
            }
        });

        if(member.length > 0) {
            const userData = {
                'id': member[0].id,
                'email': member[0].email,
                'name': member[0].name
            };
            const token = jwt.sign(userData, process.env.JWT_SECRET_TOKEN, {expiresIn: 86400});
            ctx.body = { accessToken: token, user: userData };
        } else {
            ctx.status = 403;
            ctx.body = { error: "Email or Password incorrect" };
        }
    } catch (e) {
        ctx.status = 500;
        ctx.body = { error: e };
    }
}

async function register(ctx: Context) {
    try {
        const params = ctx.request.body;
        if(params.name == "" || params.name == undefined) {
            ctx.status = 500;
            ctx.body = { error: "Please insert field name" };
            return;
        }
        if(params.email == "" || params.email == undefined) {
            ctx.status = 500;
            ctx.body = { error: "Please insert field email" };
            return;
        }
        if(params.password == "" || params.password == undefined) {
            ctx.status = 500;
            ctx.body = { error: "Please insert field password" };
            return;
        }
        console.log(params);

        const password_encrypted = crypt.createHash('md5').update(params.password).digest("hex");

        const doInsert = await models.members.create({ 
            email: params.email, 
            password: password_encrypted,
            name: params.name,
            phone: "",
            status: 1,
        });
        console.log("Insert ID:", doInsert.id);
        ctx.body = { success: "Register success", data:  doInsert.id };
    } catch (e) {
        ctx.status = 500;
        ctx.body = { error: e };
    }
}

module.exports = {
    login,
    register
}