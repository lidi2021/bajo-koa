import { Context } from "koa";

require('dotenv').config();

const initModels = require("../models/init-models").initModels;
const connection = require("../storage/connection");

const models = initModels(connection);

async function lists(ctx: Context) {
    try {
        const limit = process.env.LIMIT && parseInt(process.env.LIMIT) || 10;

        const params = ctx.request.body;
        const page = params.page == "" || params.page == 0 || params.page == undefined ? 1 : params.page;
        let offset = limit * (page - 1);
        offset = offset < 0?0:offset;

        console.log(offset);

        const listArticle = await models.article.findAll({
            attributes: ['id','title', 'description','image_url'],
            offset: offset, limit: limit,
            order: [
                ['id', 'DESC'],
            ]
        });
        const listBanner = await models.banner.findAll({
            attributes: ['id','name', 'description','image_url','url','type'],
            offset: offset, limit: limit,
            order: [
                ['id', 'DESC'],
            ]
        });

        ctx.body = {
            information: listArticle,
            banner: listBanner
        };
    } catch (e) {
        ctx.status = 500;
        ctx.body = { error: e };
    }
}

module.exports = {
    lists
}