import { Context } from "koa";

require('dotenv').config();

const initModels = require("../models/init-models").initModels;
const connection = require("../storage/connection");
const models = initModels(connection);
const { tipe_fasilitas } = require("../config");
const { pagination, imageThumb } = require("../helpers/common_helpers");
const { QueryTypes } = require('sequelize');

async function lists(ctx: Context) {
    try {
        const limit = process.env.LIMIT && parseInt(process.env.LIMIT) || 10;

        const params = ctx.request.body;
        const page = params.page == "" || params.page == 0 || params.page == undefined ? 1 : params.page;

        const paginationInfo = pagination(limit, page);

        const type = params.type == "" || params.type == undefined ? 0 : params.type;

        let { count, rows } = await models.facilities.findAndCountAll({
            attributes: ['id', 'type', 'kelas', 'nama', 'description', 'fasilitas', 'phone', 'email', 'website', 'address', 'lang', 'long', 'id_province', 'id_kab'],
            offset: paginationInfo.offset,
            limit: paginationInfo.limit,
            where: {
                status: 1,
                type: type
            },
            order: [
                ['id', 'DESC'],
            ]
        });

        rows = __getType(rows);
        rows = await __getMerchant(rows);
        rows = await __getFacilityGallery(rows);

        let pages = Math.ceil(count / paginationInfo.limit);
        ctx.body = { 'data': rows, 'current_page': paginationInfo.page, 'max_page': pages, 'total_count': count };
    } catch (e) {
        ctx.status = 500;
        ctx.body = { error: e };
    }
}

async function detail(ctx: Context) {

}

async function __getFacilityGallery(listsFacility: any) {
    // if (is_array($result) && count($result) > 0) {
    //     foreach ($result as $key => $value) {
    //         $gallery_db = $this->db->query("SELECT path,title FROM " . $this->facilities_img_table . " WHERE id_facilities = " . $value['id'] . " ")->first_row('array');
    //         if ($gallery_db != null) {
    //             $result[$key]['gallery'][0]['image'] = $gallery_db['path'] != null && $gallery_db['path'] != '' ? thumb_image($gallery_db['path'], '320x200', 'facility') : '';
    //         } else {
    //             $result[$key]['gallery'][0]['image'] = $this->webconfig['back_images'] . "no-image.jpg";
    //         }
    //     }
    // }

    try {

        if (listsFacility.length > 0) {
            for (let index = 0; index < listsFacility.length; index++) {
                const image = await models.facilities_img.findOne({
                    attributes: ['path','title'],
                    where: {
                        id_facilities: listsFacility[index].id
                    }
                });

                listsFacility[index].gallery = [
                    {
                        image: image != null ? imageThumb(image.path, '320x200', 'facility') : ''
                    }
                ]
            }
            return listsFacility;
        }
    } catch (err) {
        console.log(err);
    }

    return listsFacility;
}

async function __getMerchant(listsFacility: any) {
    try {

        if (listsFacility.length > 0) {
            for (let index = 0; index < listsFacility.length; index++) {
                const merchant = await connection.query("SELECT merchants.* FROM facilities_merchants LEFT JOIN merchants ON facilities_merchants.id_merchants = merchants.id WHERE facilities_merchants.id_facilities = :id_facilities", {
                    model: models.merchants,
                    mapToModel: true,
                    replacements: { id_facilities: listsFacility[index].id },
                    type: QueryTypes.SELECT
                });
                listsFacility[index].merchant = {
                    nama: merchant[0].nama,
                    description: merchant[0].description,
                    phone: merchant[0].phone,
                    email: merchant[0].email,
                    website: merchant[0].website
                }
            }
            return listsFacility;
        }
    } catch (err) {
        console.log(err);
    }

    return listsFacility;
}

function __getType(listsFacility: any) {
    let lists: any[] = [];
    if (listsFacility.length > 0) {
        listsFacility.map((data: any) => {
            lists.push({
                id: data.id,
                type: data.type,
                kelas: data.kelas,
                nama: data.nama,
                description: data.description,
                fasilitas: data.fasilitas,
                phone: data.phone,
                email: data.email,
                website: data.website,
                address: data.address,
                lang: data.lang,
                long: data.long,
                id_province: data.id_province,
                id_kab: data.id_kab,
                tipe_str: data.type != null && data.type != undefined ? tipe_fasilitas[data.type] : ''
            })
        });
    }

    return lists.length > 0 ? lists : listsFacility;
}

module.exports = {
    lists,
    detail
}